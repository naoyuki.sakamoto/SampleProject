### GIT勉強用プロジェクト（Java Springテンプレートプロジェクトから作成）

このプロジェクトは自分の勉強用に作りました。

このプロジェクトはGitLab [プロジェクトテンプレート]（https://docs.gitlab.com/ee/gitlab-basics/create-project.html）に基づいています。

改善は[オリジナルプロジェクト]（https://gitlab.com/gitlab-org/project-templates/spring）で提案することができます。

### CI / CDとAuto DevOps

このテンプレートは[Auto DevOps]（https://docs.gitlab.com/ee/topics/autodevops/）と互換性があります。

このプロジェクトでAuto DevOpsが有効になっていない場合は、プロジェクト設定で[オンにする]（https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops）することができます。
